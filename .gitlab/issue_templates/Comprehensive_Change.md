## Business Technology Comprehensive Change Management Process

These issues should help us understand how an adjustment or change to the way we currently do things will impact processes, systems, and employees within the organization. Please make sure to include all stakeholders or teams/departments affected by this change in this issue and be sure to detail in the description how this will impact them.

**IMPORTANT NOTE** Prior to logging this issue, ensure that you are selecting the correct Change Type. If you are unsure of what type this change is, please review the [Change Management README](https://gitlab.com/gitlab-com/business-ops/change-management/-/blob/master/README.md) prior to logging this issue.

#### Comprehensive Change

A **comprehensive change** is *high risk, high impact,* or has a *more complex procedure.*
* All changes to **financially significant applications** also are considered comprehensive due to the type of systems that they affect and the potential impact that could occur if there is an issue.
* **Infrastructure changes** are also considered comprehensive.
* **CAB approval must also be obtained** in order to proceed with implementation of a comprehensive change.

#### Please use this template to track change(s) to a current third-party system GitLab uses internally
* [ ] Check the [tech stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) for the admin of the system being proposed and please assign appropriately
* [ ] Please make sure to share this issue with all relevant stakeholders or teams/departments.
* [ ] Mention `@gitlab-com/gl-security/appsec` in comments for [application security reviews](https://about.gitlab.com/handbook/engineering/security/#internal-application-security-reviews)
* [ ] Admin: Review the requested change and comment on feasibility
* [ ] Security: Review for potential security issues
* [ ] Change approval is obtained (`cannot be same person applying the change`)
* [ ] Mention `@gitlab-com/business-ops/bizops-bsa`
* [ ] If this change is related to adding a new application to Okta, please include a comment with the names of the Okta App Admins for the tool you are rolling out.
   * [ ] Link completed Okta Add Application issue `here`. If an Okta Add Application issue has not been created, please [create an issue request](https://gitlab.com/gitlab-com/business-ops/change-management/-/issues/new?issuable_template=change_management_okta) to begin this process. 

### What type of change is this?

* [ ] Automated updates from the vendor
* [ ] Customized changes - either performed by us or by the vendor
* [ ] Access Request Role Based Entitlement Access Modification
   * [ ] Additional approval from a manager and/or director from the department the role belongs to. [`add team member handle here`]

### Please list the reason for this change

* [Example: Change is needed to automate manual processes that will impact multiple departments.]

### Please list the system and how it currently works

* [Example: Update zoom to auto-record meetings]

### Use this section to describe the proposed change and the business need/benefit for applying the change

* [Example: Make a change to admin settings in zoom to have every meeting started auto record, then the person hosting can turn off if needed. This change follows our transparency value, etc.]

* #### Automated Updates

   * [Example: add release notes provided by the vendor for the application patch/update]

### Impact this change would have on business

* [Example: Would allow everyone a chance to record and upload meetings for transparency. Would require everyone to take an extra step in turning off though when the meeting starts if the intention is not to record.]

### Testing Procedures

* [Clear detailed documentation of how the change will be tested. If testing is not applicable, detailed explanation of why testing is not required.]

* [ ] Yes this change requires testing
   * [ ] [link to testing issue]
* [ ] No this change does not require testing

### Change backout procedures

* [Example: revert applied changes back to previous version level of "x", or restore the initial configuration file to the switch or the firewall]

### List any handbook pages where this change would require an update to the handbook

* [Example link.]

### Plan to announce change to GitLab team members

* [ ]  [No announcements required?]
* [ ]  [Handbook?]
* [ ]  [Slack?]
* [ ]  [Company/Department calls?]

## Review and Approvals

### Peer Review

Peer Reviews are performed by a peer of the Change Requestor and is intended to identify any potential issues with the planned change or change process.

* [ ] Peer Review completed by `[add team member handle here]`

### Team Member Enablement Management Approval

Approval by Team Member Enablement is required in order to proceed.
* [ ] Once this issue has been filled out, tag `@pkaldis` in the comment section for review and approval.

### CAB Approval

* [ ] Review and Approval must be obtained by the Change Approval Board in order to proceed with implementation of a comprehensive change. No further action is needed from you as the CAB will review and approve or comment the reason why this change can not be implemented. 


---

### Customized changes (`configuration changes, setting changes, and/or customization done by vendor`)

	_Please add content to highlighted syntax below_

   * [ ] attach, link, or provide internal change management log `[please add here]`
   * [ ] who performed the change? `[add team member handle here]`
   * [ ] was the change approved? `[yes/no]`
   * [ ] who approved the change? `[add team member handle here]`
   * [ ] was the change performed by the appropriate party? `[yes/no]`
   * [ ] if the vendor performed the change, was it implemented appropriately? `[yes/no]`
   * [ ] if applicable, was testing performed? `[yes/no]`
   * [ ] is there a backout plan? `[yes/no]`
   * [ ] segregation of duties - did all team members associated with this change perform associated tasks that are specific to their role (i.e. team member who made change wasn't also the approver) `[yes/no]`


---
---

Business Technology Change Management: @kxkue

/label  ~"Change Management" ~"CMT::Comprehensive Change" ~"CAB Approval" 
